export class Events{
    /**
     * 
     * @param {String} title 
     * @param {String} description 
     * @param {String} location 
     * @param {String} start 
     * @param {String} startTime 
     * @param {String} endTime 
     */
    constructor(title,description, location, start){
        this.title=title;
        this.description=description;
        this.location=location;
        this.start=start;
        //this.startTime=startTime;
        //this.endTime=endTime;
    }
}